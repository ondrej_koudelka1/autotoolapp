import classnames from 'classnames';
import moment from 'moment';

import './Validator.css';

export class Validator {
	constructor(fields) {
		this.fields = fields || [];
		this.validationResults = this.getInitialResults();
		this.form = {};
	}

	validate(form) {
		this.validationResults = this.getInitialResults();
		this.form = form;
		let isFormValid = true;

		if (this.form.submitted) {
			this.fields.forEach(({ field, rules }) => {
				if (this.form[field] === undefined) {
					throw new Error(`${field} is not defined in State.`);
				}

				const value = this.form[field];

				rules.forEach(({ method, validWhen, errorMsg, arg }) => {
					if (this.validationResults[field].isInvalid === false) {
						const isInvalid =
							validWhen !== this.check(method, arg, value);
						isFormValid = !isInvalid && isFormValid;
						this.validationResults[field] = {
							isInvalid,
							errorMsg,
						};
					}
				});
			});
		}

		return isFormValid;
	}

	getClassNames = field =>
		classnames(
			'form-control',
			this.validationResults[field].isInvalid ? 'is-invalid' : null,
		);

	getErrorMessage = field =>
		this.validationResults[field].isInvalid
			? this.validationResults[field].errorMsg
			: '';

	getInitialResults() {
		const validationResults = {};
		this.fields.map(({ field }) => {
			return (validationResults[field] = {
				isInvalid: false,
				errorMsg: '',
			});
		});

		return validationResults;
	}

	check = (method, arg, value) => {
		switch (method) {
			case IS_EMPTY:
				return isEmpty(value);
			case IS_EMAIL:
				return isEmail(value);
			case MIN_LENGTH:
				return minLength(arg, value);
			case IS_EQUAL_TO_FIELD:
				return isEqual(this.form[arg].toString(), value);
			case IS_DATE:
				return isDate(value);
			case IS_TIME:
				return isTime(value);
			case MIN_DATE:
				return minDate(arg, value);
			case MIN_TIME:
				return minTime({dateField: this.form[arg.field], minTime: arg['minTime']}, value);
			default:
				throw new Error(`${method} is not defined in Validator.`);
		}
	};
}

export const IS_EMPTY = 'isEmpty';
export const IS_EMAIL = 'isEmail';
export const MIN_LENGTH = 'minLength';
export const IS_EQUAL_TO_FIELD = 'isEqualToField';
export const IS_DATE = 'isDate';
export const IS_TIME = 'isTime';
export const MIN_DATE = 'minDate';
export const MIN_TIME = 'minTime';

export const isEmpty = value => {
	return !value || value.length === 0;
};

export const isEmail = value => {
	const emailRegex = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	return emailRegex.test(value);
};

export const minLength = (arg, value) => {
	return value.length >= arg;
};

export const isEqual = (firstValue, secondValue) => {
	return firstValue === secondValue;
};

export const isDate = value => {
	return value && (moment(value).isValid() || moment(value, 'YYYY-MM-DD', true).isValid());
};

export const isTime = value => {
	return value && (moment(value).isValid() || moment(value, 'HH:mm', true).isValid());
};

export const minDate = (dateArg, dateGiven) => {
	return isDate(dateGiven) && moment(dateGiven).isSameOrAfter(moment(dateArg).format('YYYY-MM-DD'));
};

export const minTime = ({dateField, timeArg}, timeGiven) => {
	const selectedDate = moment(dateField);

	if (!selectedDate.isValid() && !isTime(timeGiven)) {
		return false;
	} else if(selectedDate.isSame(new Date(), 'day')) {
		return moment(timeGiven, 'HH:mm').isSameOrAfter(new Date());
	} else {
		return true;
	}
};
