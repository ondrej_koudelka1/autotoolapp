import thunk from 'redux-thunk';
import { applyMiddleware, createStore } from 'redux';
import { persistStore } from 'redux-persist';
import { createRootReducer } from './createRootReducer';

export const configureStore = () => {
	const rootReducer = createRootReducer();

	const store = createStore(
		rootReducer,
		window.__REDUX_DEVTOOLS_EXTENSION__ &&
			window.__REDUX_DEVTOOLS_EXTENSION__(),
		applyMiddleware(thunk),

	);

	const persistor = persistStore(store);

	return { persistor, store };
};
