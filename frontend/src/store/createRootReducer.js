import { combineReducers } from 'redux';
import { persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage';

const persistConfig = {
	key: 'root',
	storage,
	whitelist: [],
};

export const createRootReducer = () => {
	const rootReducer = combineReducers({});

	return persistReducer(persistConfig, rootReducer);
};
