export default (axios, store) => {
	axios.interceptors.request.use(req => {
		return req;
	});

	axios.interceptors.response.use(
		res => res,
		errorResponse => {
			if (errorResponse.response.status === 401) {
				const { error } = errorResponse.response.data;
			}
			return Promise.reject(errorResponse);
		},
	);
};
