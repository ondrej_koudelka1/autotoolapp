import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { Button } from 'bootstrap';
import { ListableCriteria } from '../../components/Criteria/ListableCriteria';
import { CustomLabel } from '../../components/Criteria/CustomLabel';

import FontIcon from '../../components/FontIcon/FontIcon';
import Axios from '../../axios';
import PageTitle from '../../components/PageTitle/PageTitle';

import './ToolDetail.css';
import '../Criteria/Criteria.css';

class ToolDetail extends Component {
	constructor(props) {
		super(props);

		this.state = {
			toolType: null,
			crits: [],
			values: [],
		};
	}

	render() {
		let crits = this.props.tools.criterias;
		let tools = this.props.tools;
		let id = this.props.tools.toolId;
		let type = this.props.tools.toolType;
		let toolItemHref = '#toolItem' + id;
		let toolItemId = 'toolItem' + id;
		let score;

		if (this.props.score != null) {
			score = (
				<div className="col text-truncate">
					<label>Skóre: {this.props.score}</label>
				</div>
			);
		}

		return (
			<div id="">
				<div id="accordion" className="accordion">
					<button
						className="toolDetail btn col-sm-12"
						data-toggle="collapse"
						type="button"
						aria-controls="collapseExample"
						aria-expanded="true"
						id={id}
						href={toolItemHref}
					>
						<div className="row">
							<div className="col text-truncate">
								<label className="tool-header">
									{tools.toolName}
								</label>
							</div>
							{score}
						</div>
					</button>

					<div
						id={toolItemId}
						className="collapse"
						data-parent="#accordion"
					>
						<label className="criteria-name">Typ</label>{' '}
						<CustomLabel value={type} />
						{crits ? (
							crits.map(crit => (
								<ListableCriteria
									key={crit.CRITERIA_ID}
									criteria={crit}
								/>
							))
						) : (
							<div />
						)}
					</div>
				</div>
			</div>
		);
	}
}

export default ToolDetail;
