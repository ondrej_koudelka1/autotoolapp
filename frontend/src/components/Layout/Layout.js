import React, { Component } from 'react';
import { connect } from 'react-redux';

import ToolBar from '../Navigation/Navigation';

import './Layout.css';

class Layout extends Component {
	render() {
		return (
			<div>
				<div className="layout">
					<ToolBar />
					{this.props.children}
				</div>
			</div>
		);
	}
}

const mapStateToProps = state => {
	return {};
};

export default connect(mapStateToProps)(Layout);
