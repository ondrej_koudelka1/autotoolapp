import React from 'react';

import './PageTitle.css';

export default (props) => (
	<div className="my-jumbotron my-title">
		<h1>{props.children}</h1>
		<hr/>
	</div>
);
