import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';

import Item from './MenuItem/MenuItem';
import Logo from './Logo/index';
import UserSetting from './UserSetting/UserSetting';

import './Navigation.css';

class Navigation extends Component {
	render() {
		return (
			<nav
				id="toolbar"
				className="nav navbar navbar-expand-lg navbar-light"
			>
				<div
					className="collapse navbar-collapse navbar-expand-lg"
					id="navbarToggler"
				>
					<ul
						className="nav navbar-nav menu-items flex-fill w-100"
						id="navbarItems"
					>
						<Item path="/">Úvod</Item>
						<Item path="/addTool">Přidat nástroj</Item>
						<Item path="/chooseTool">Vybrat nástroj</Item>
						<Item path="/ToolList">Seznam nástrojů</Item>
					</ul>
				</div>
			</nav>
		);
	}
}

const mapStateToProps = state => {
	return {};
};

export default withRouter(
	connect(
		mapStateToProps,
		null,
	)(Navigation),
);
