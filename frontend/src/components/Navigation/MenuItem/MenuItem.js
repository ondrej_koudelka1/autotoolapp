import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';
import cn from 'classnames';

import './MenuItem.css';

class MenuItem extends Component {
	render() {
		const item = this.props.path ? (
			<NavLink exact to={this.props.path} className="nav-link" activeClassName="active">
				{this.props.children}
			</NavLink>
		) : (
			<div className="nav-link">{this.props.children}</div>
		);

		const classNames = cn('nav-item', this.props.classNames);
		return <li className={classNames}>{item}</li>;
	}
};

export default MenuItem;
