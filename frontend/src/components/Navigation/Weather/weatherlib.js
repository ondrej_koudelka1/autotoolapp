let http = require('http');

let apiKey = '4e8c7851b04d4f2abeb91915180211';

let options = {
	host: 'api.apixu.com',
	port: 80,
	path: '/v1/current.json?key=' + apiKey + '&q=',
	method: 'GET',
};

exports.currentWeather = function currentWeather(query, callback) {
	options.path = '/v1/current.json?key=' + apiKey + '&q=' + query;
	let data = {};

	http.request(options, function(res) {
		res.setEncoding('utf8');

		res.on('data', function(chunk) {
			data = JSON.parse(chunk)['current'];
		});

		res.on('end', function(chunk) {
			callback(data);
		});
	})
		.on('error', function(err) {
			console.error('Error with the request:', err.message);
		})
		.end();
};
