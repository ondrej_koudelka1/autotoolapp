import React from 'react';
import { connect } from 'react-redux';
import weather from './weatherlib';

import './Weather.css';

class Weather extends React.PureComponent {
	constructor(props) {
		super(props);

		this.state = {
			temperature: '-',
			perceivedTemperature: '-',
			humidity: '-',
			text: '',
			icon: '',
		};
	}

	componentDidMount() {
		this.updateWeather();
	}

	componentDidUpdate() {
		this.updateWeather();
	}

	updateWeather() {
		const {lat, lng} = this.props.startingPosition || {};
		if (lat && lng) {
			this.coordinatesWereSet(lat, lng);
		}
	}

	coordinatesWereSet(lat, lng) {
		weather.currentWeather(lat + ',' + lng, data => {
			if (data) {
				this.setState({
					temperature: data.temp_c,
					perceivedTemperature: data.feelslike_c,
					humidity: data.humidity,
					text: data.condition.text,
					icon: 'http:' + data.condition.icon,
				});
			}

		});
	}

	render() {
		return (
			<div className="weather">
				{this.state.temperature} °C{' '}
				{this.state.icon ? (
					<img
						alt="weather icon"
						className="weather-icon"
						src={this.state.icon}
					/>
				) : (
					''
				)}
			</div>
		);
	}
}

const mapStateToProps = state => {
	const {startingPosition} = state.auth;
	return {
		startingPosition,
	};
};

export default connect(
	mapStateToProps,
)(Weather);
