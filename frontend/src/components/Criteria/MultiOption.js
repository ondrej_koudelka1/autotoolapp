import React from 'react';

export const MultiOption = ({ value }) => {
	let optionValue = value.CRIT_VALUE;
	let optionId = value.CRIT_VALUES_ENUM_ID;

	return (
		<div>
			<input type="checkbox" optionid={optionId} />
			<label>{optionValue}</label>
		</div>
	);
};
