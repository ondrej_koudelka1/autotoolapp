import React, { Component } from 'react';
import Axios from '../../axios';
import { MultiOption } from './MultiOption';

import './Criteria.css';

export class MultiPicker extends Component {
	constructor(props) {
		super(props);

		this.state = {
			values: this.props.values,
			critId: this.props.critId,
			valuesMulti: [],
		};

		// this.handleChange = this.handleChange.bind(this);
	}

	onClick = () => {
		let value = document.getElementById('NewCritValue' + this.state.critId)
			.value;
		let critIdCreated;
		Axios.post(`/criteria/addCriteriaValue`, {
			critId: this.state.critId,
			value: value,
		}).then(response => {
			critIdCreated = response.data.id;

			this.setState({
				values: [
					...this.state.values,
					{ CRIT_VALUE: value, CRIT_VALUES_ENUM_ID: critIdCreated },
				],
			});
		});

		document.getElementById('NewCritValue' + this.state.critId).value = '';
	};

	handleChange = checkbox => {
		let critId = checkbox.target.getAttribute('optionid');
		if (checkbox.target.checked == true) {
			this.props.addCriteriaValue(critId, 'true');
		} else {
			this.props.addCriteriaValue(critId, 'false');
		}
	};

	render() {
		let newCritValueId = 'NewCritValue' + this.state.critId;
		return (
			<div className="criteria">
				{this.state.values ? (
					this.state.values.map(value => (
						<div
							key={value.CRIT_VALUES_ENUM_ID}
							value={value}
							optionid={value.CRIT_VALUES_ENUM_ID}
							className="row crit-option form-group"
						>
							<input
								type="checkbox"
								onClick={this.handleChange}
								optionid={value.CRIT_VALUES_ENUM_ID}
								className="col-sm-1"
							/>
							<label className="crit-label">
								{value.CRIT_VALUE}
							</label>
						</div>
					))
				) : (
					<div />
				)}
				<div className="input-group row add-option">
					<div className="col-sm-1" />
					<input
						type="text"
						className="form-control col-sm-4"
						aria-describedby="basic-addon2"
						id={newCritValueId}
					/>
					<div class="input-group-append">
						<button
							className="btn btn-primary"
							onClick={this.onClick}
							type="button"
						>
							Přidat možnost
						</button>
					</div>
				</div>
			</div>
		);
	}
}
