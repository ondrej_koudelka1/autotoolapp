import React, { Component } from 'react';
import Axios from '../../axios';
import { MultiOption } from './MultiOption';
import './Criteria.css';

export class NumberInputChoose extends Component {
	constructor(props) {
		super(props);

		this.state = {
			critId: this.props.critId,
			values: [],
			optionId: '',
		};
	}

	onFocusOut = () => {
		let price = document.getElementById('priceId');
		let weight = document.getElementById('priceWeightId');
		console.log(price.value);
		if (price.value) {
			weight.removeAttribute('disabled');
		} else {
			weight.setAttribute('disabled', 'disabled');
		}
	};

	validateNumber = event => {
		event.target.value = event.target.value.replace(/\D/, '');
	};

	render() {
		let newCritValueId = 'NewCritValue' + this.state.critId;
		return (
			<div className="criteria">
				<div className="col-sm-12 price-group form-group">
					<input
						type="text"
						min="1"
						id="priceId"
						onBlur={this.onFocusOut}
						onKeyUp={this.validateNumber}
						className="price-input col-sm-3 form-control"
					/>
					<span className="col-sm-6" />
					<input
						type="text"
						min="1"
						id="priceWeightId"
						placeholder="Váha"
						disabled="disabled"
						onKeyUp={this.validateNumber}
						className="price-input col-sm-2 form-control"
					/>
				</div>
			</div>
		);
	}
}
