import React, { Component } from 'react';

export class CustomLabel extends Component {
	constructor(props) {
		super(props);
	}

	render() {
		let optionValue = this.props.value;
		if (optionValue == 'true') {
			optionValue = 'Ano';
		} else if (optionValue == 'false') {
			optionValue = 'Ne';
		}

		return <label className="col-sm-10">{optionValue}</label>;
	}
}
