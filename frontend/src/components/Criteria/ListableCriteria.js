import React, { Component } from 'react';
import { BooleanPicker } from './BooleanPicker';
import { MultiPicker } from './MultiPicker';
import { NumberInputChoose } from './NumberInputChoose';
import { CustomLabel } from './CustomLabel';

import './Criteria.css';

export class ListableCriteria extends Component {
	constructor(props) {
		super(props);

		this.state = {
			selectedValue: null,
			selectedOption: null,
			isHidden: true,
		};
	}

	render() {
		const name = this.props.criteria.NAME;
		const values = this.props.criteria.critValue;

		let picker;
		picker = (
			<div>
				{values ? (
					values.map(value => (
						<div className="row">
							<CustomLabel value={value.CRIT_VALUE} />
						</div>
					))
				) : (
					<div />
				)}
			</div>
		);

		return (
			<div className="listable-criteria">
				<label className="criteria-name">{name}</label>
				{picker}
			</div>
		);
	}
}
