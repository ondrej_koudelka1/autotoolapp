import React, { Component } from 'react';

import { CustomOption } from './CustomOption';

export class BooleanPicker extends Component {
	constructor(props) {
		super(props);
		this.state = {
			values: this.props.values,
			selectedOption: null,
		};
	}

	handleChange = event => {
		let index = event.target.selectedIndex;
		let optionElement = event.target.childNodes[index];
		let value = optionElement.getAttribute('optionid');
		if (this.state.selectedOption != null) {
			let selectedOption = this.state.selectedOption;
			this.props.addCriteriaValue(selectedOption, 'false');
		}

		this.setState({
			selectedOption: value,
		});
		this.props.addCriteriaValue(value, 'true');
	};

	render() {
		return (
			<div className="criteria">
				<div className="col-sm-1" />
				<select
					className="browser-default custom-select col-sm-4"
					onChange={this.handleChange}
				>
					<option disabled selected value />
					{this.state.values ? (
						this.state.values.map(value => (
							<CustomOption
								key={value.CRIT_VALUES_ENUM_ID}
								value={value}
								optionid={value.CRIT_VALUES_ENUM_ID}
							/>
						))
					) : (
						<div />
					)}
				</select>
			</div>
		);
	}
}
