import React, { Component } from 'react';
import Axios from '../../axios';
import { MultiOption } from './MultiOption';

export class NumberInput extends Component {
	constructor(props) {
		super(props);

		this.state = {
			critId: this.props.critId,
			values: [],
			optionId: '',
		};
	}

	onFocusOut = () => {
		let value = document.getElementById('NewCritValue' + this.state.critId)
			.value;
		let critIdCreated;
		Axios.post(`/criteria/addCriteriaValue`, {
			critId: this.state.critId,
			value: value,
		}).then(response => {
			critIdCreated = response.data.id;
			this.setState({
				values: [
					{ CRIT_VALUE: value, CRIT_VALUES_ENUM_ID: critIdCreated },
				],
				optionId: critIdCreated,
			});
			this.props.addCriteriaValue(critIdCreated, value);
		});
	};

	render() {
		let newCritValueId = 'NewCritValue' + this.state.critId;
		return (
			<div className="criteria ">
				<div className="col-sm-6 price-group form-group">
					<input
						type="number"
						min="1"
						optionid={this.state.optionId}
						id={newCritValueId}
						onBlur={this.onFocusOut}
						className="price-input col-sm-6 form-control"
					/>
				</div>
			</div>
		);
	}
}
