import React, { Component } from 'react';
import { BooleanPicker } from './BooleanPicker';
import { MultiPicker } from './MultiPicker';
import { NumberInput } from './NumberInput';

import './Criteria.css';

export class EditableCriteria extends Component {
	constructor(props) {
		super(props);
	}

	render() {
		const name = this.props.criteria.NAME;
		const value = this.props.criteria.critValue;
		const type = this.props.criteria.CritType.NAME;
		const critId = this.props.criteria.CRITERIA_ID;
		const desc = this.props.criteria.DESC;

		let picker;
		if (type == 'boolean') {
			picker = (
				<BooleanPicker
					values={value}
					critId={critId}
					addCriteriaValue={this.props.addCriteriaValue}
				/>
			);
		} else if (type == 'enum') {
			picker = (
				<MultiPicker
					values={value}
					critId={critId}
					addCriteriaValue={this.props.addCriteriaValue}
				/>
			);
		} else if (type == 'integer') {
			picker = (
				<NumberInput
					critId={critId}
					addCriteriaValue={this.props.addCriteriaValue}
				/>
			);
		}

		return (
			<div>
				<label className="criteria-name">{name}</label>
				<p>{desc}</p>
				{picker}
			</div>
		);
	}
}
