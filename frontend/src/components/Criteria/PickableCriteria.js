import React, { Component } from 'react';
import { BooleanPicker } from './BooleanPicker';
import { MultiPicker } from './MultiPicker';
import { NumberInputChoose } from './NumberInputChoose';

import './Criteria.css';

export class PickableCriteria extends Component {
	constructor(props) {
		super(props);

		this.state = {
			selectedValue: null,
			selectedOption: null,
			isHidden: true,
		};
	}

	validateNumber = event => {
		event.target.value = event.target.value.replace(/\D/, '');
	};

	handleChangeRadio = event => {
		const critId = event.target.getAttribute('optionid');
		let test = event.target.nextSibling.innerHTML;
		console.log(test);
		let newValue = { value: true, weight: 10 };
		let oldValue = this.state.selectedValue;
		let oldOption = this.state.seletedOption;
		if (oldValue != null) {
			let selectedValue = this.state.selectedValue;
			let selectedOption = this.state.selectedOption;
			this.props.addCriteriaValue(selectedOption, { value: false });
		}
		this.setState({
			selectedValue: newValue,
			selectedOption: critId,
		});

		this.props.addCriteriaValue(critId, newValue);
	};

	handleChangeCheckbox = checkbox => {
		let critId = checkbox.target.getAttribute('optionid');
		let weight = checkbox.target.nextSibling.nextSibling;

		if (checkbox.target.checked == true) {
			this.props.addCriteriaValue(critId, { value: 'true', weight: 10 });
			weight.removeAttribute('disabled');
		} else {
			this.props.addCriteriaValue(critId, { value: 'false' });
			weight.setAttribute('disabled', 'disabled');
		}
	};

	render() {
		const name = this.props.criteria.NAME;
		const values = this.props.criteria.critValue;
		const type = this.props.criteria.CritType.NAME;
		const critId = this.props.criteria.CRITERIA_ID;
		const desc = this.props.criteria.CHOOSE_DESC;

		let picker;
		if (type == 'boolean') {
			picker = (
				<div className="criteria">
					{values ? (
						values.map(value => (
							<div
								key={value.CRIT_VALUES_ENUM_ID}
								value={value}
								className="row crit-option form-group"
							>
								<input
									type="checkbox"
									onClick={this.handleChangeCheckbox}
									optionid={value.CRIT_VALUES_ENUM_ID}
									name={name}
									className="col-sm-1"
								/>
								<label className="col-sm-8 crit-label">
									Ano
								</label>
								<input
									type="text"
									min="1"
									placeholder="Váha"
									disabled="disabled"
									weightid={value.CRIT_VALUES_ENUM_ID}
									className="col-sm-2 form-control"
									onKeyUp={this.validateNumber}
								/>
							</div>
						))
					) : (
						<div />
					)}
				</div>
			);
		} else if (type == 'enum') {
			picker = (
				<div className="criteria">
					{values ? (
						values.map(value => (
							<div
								key={value.CRIT_VALUES_ENUM_ID}
								value={value}
								className="row crit-option form-group"
							>
								<input
									type="checkbox"
									onClick={this.handleChangeCheckbox}
									optionid={value.CRIT_VALUES_ENUM_ID}
									className="col-sm-1"
								/>
								<label className="col-sm-8 crit-label">
									{value.CRIT_VALUE}
								</label>
								<input
									placeholder="Váha"
									disabled="disabled"
									type="number"
									min="1"
									max="10"
									weightid={value.CRIT_VALUES_ENUM_ID}
									className="col-sm-2 form-control"
									onKeyUp={this.validateNumber}
								/>
							</div>
						))
					) : (
						<div />
					)}
				</div>
			);
		} else if (type == 'integer') {
			picker = (
				<NumberInputChoose
					critId={critId}
					addCriteriaValue={this.props.addCriteriaValue}
					className="criteria"
				/>
			);
		}

		return (
			<div>
				<label className="criteria-name">{name}</label>
				<p>{desc}</p>
				{picker}
			</div>
		);
	}
}
