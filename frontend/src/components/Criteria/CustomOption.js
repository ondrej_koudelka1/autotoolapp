import React, { Component } from 'react';

export class CustomOption extends Component {
	constructor(props) {
		super(props);
	}

	render() {
		let optionValue = this.props.value.CRIT_VALUE;
		let optionId = this.props.value.CRIT_VALUES_ENUM_ID;
		if (optionValue == 'true') {
			optionValue = 'Ano';
		} else if (optionValue == 'false') {
			optionValue = 'Ne';
		}

		return <option optionId={optionId}>{optionValue}</option>;
	}
}
