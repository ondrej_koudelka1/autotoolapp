import React, { Component } from 'react';
import { Switch, Route, Redirect, withRouter } from 'react-router-dom';
import { connect } from 'react-redux';

import Layout from '../components/Layout/Layout';
import Home from '../scenes/Home/Home';
import NewTool from '../scenes/NewTool/NewTool';
import ChooseTool from '../scenes/ChooseTool/ChooseTool';
import ResultPage from '../scenes/ResultPage/ResultPage';
import ToolList from '../scenes/ToolList/ToolList';

class AppRoutes extends Component {
	componentDidMount() {}

	render() {
		let routes = null;

		routes = (
			<Layout>
				<Switch>
					<Route path="/" exact component={Home} />
					<Route path="/addTool" exact component={NewTool} />
					<Route path="/chooseTool" exact component={ChooseTool} />
					<Route path="/ResultPage" exact component={ResultPage} />
					<Route path="/ToolList" exact component={ToolList} />
					<Redirect to="/" />
				</Switch>
			</Layout>
		);

		return routes;
	}
}

const mapStateToProps = state => {
	return {};
};

const mapDispatchToProps = dispatch => {
	return {};
};

export default withRouter(
	connect(
		mapStateToProps,
		mapDispatchToProps,
	)(AppRoutes),
);
