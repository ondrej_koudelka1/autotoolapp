import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { Button } from 'bootstrap';
import { ListableCriteria } from '../../components/Criteria/ListableCriteria';

import FontIcon from '../../components/FontIcon/FontIcon';
import Axios from '../../axios';
import PageTitle from '../../components/PageTitle/PageTitle';
import ToolDetail from '../../components/ToolDetail/ToolDetail';

class ToolList extends Component {
	constructor(props) {
		super(props);

		this.state = {
			toolType: null,
			tools: [],
			values: [],
		};
	}

	componentDidMount() {
		Axios.get('/testTool/getAllTools').then(response => {
			this.setState({
				tools: response.data.result,
			});
		});
	}

	render() {
		const tools = this.state.tools;

		return (
			<div id="tools" className="container col-md-4">
				<div className="white-box">
					<h1>Seznam nástrojů</h1>
					{this.state.tools ? (
						tools.map(tool => (
							<ToolDetail key={tool.toolId} tools={tool} />
						))
					) : (
						<div />
					)}
				</div>
			</div>
		);
	}
}

export default ToolList;
