import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { Button } from 'bootstrap';

import FontIcon from '../../components/FontIcon/FontIcon';
import Axios from '../../axios';
import PageTitle from '../../components/PageTitle/PageTitle';
import './Home.css';
class Home extends Component {
	state = {
		events: null,
		key: null,
	};

	eventFilters = {};

	componentDidMount() {}

	render() {
		return (
			<div className="container white-box col-sm-4">
				<h1>Úvod</h1>
				<p className="appdesc">
					Tato aplikace je určená pro podporu rozhodování ve výběru
					nástroje pro automatizované testování. V rámci aplikace je
					momentálně pokryto funční testování GUI a API. Při výběru
					nástroje je proto potřeba specifikovat jaký typ nástroje
					hledáte.
				</p>
				<h3>Přidání nástroje</h3>
				<p className="appdesc">
					Aplikace obsahuje nástroje, které byly ohodnoceny na základě
					definovaných kritérií. Pokud zde nějaký nástroj chybí,
					můžete ho sami přidat v sekci
					<a href="/addTool"> Přidat nástroj</a>.
				</p>
				<h3>Výběr nástroje</h3>
				<p className="appdesc">
					Při výběru nástroje si sami určíte, podle jakých kritérií
					chcete nástroj vybírat, a jak důležitá tato kritéria jsou
					zadáním váhy kritéria. Váhy kritérií mohou nabývat čísel
					1-n, a platí, že čím vyšší číšlo, tím důležitější kritérium.
					Aplikace pak pro každý nástroj a pro každé kritérium určí,
					zda daný nástroj splňuje dané kritérium. Pokud ano, pak se
					váha tohoto kritéria přičte do finálního skóre daného
					nástroje.
				</p>
				<p className="appdesc">
					Následně se zobrazí seznam nástrojů, který je seřazen od
					nejvyššího skóre po nejnižší. Každý nástroj v seznamu lze
					rozkliknout, a zobrazit si kompletní výčet jeho specifik.
				</p>
				<h3>Seznam nástrojů</h3>
				<p className="appdesc">
					Pokud se chcete pouze podívat, jaké nástroje jsou zde
					uložené, a jaká mají specifika, můžete tak učinit v sekci
					<a href="/ToolList"> Seznam nástrojů</a>.
				</p>
			</div>
		);
	}
}

export default Home;
