import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { Button } from 'bootstrap';

import FontIcon from '../../components/FontIcon/FontIcon';
import Axios from '../../axios';
import PageTitle from '../../components/PageTitle/PageTitle';
import ToolDetail from '../../components/ToolDetail/ToolDetail';

class ResultPage extends Component {
	state = {
		tools: null,
	};

	componentDidMount() {
		this.setState({
			tools: this.props.location.state.tools,
		});
	}

	render() {
		let tools = this.state.tools;
		return (
			<div className="container col-md-4 white-box">
				<h1>Vyhodnocení</h1>
				{tools ? (
					tools.map(tool => (
						<ToolDetail
							key={tool.toolId}
							tools={tool}
							score={tool.toolScore}
						/>
					))
				) : (
					<div />
				)}
			</div>
		);
	}
}

export default ResultPage;
