import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { Button } from 'bootstrap';
import { PickableCriteria } from '../../components/Criteria/PickableCriteria';

import FontIcon from '../../components/FontIcon/FontIcon';
import Axios from '../../axios';
import PageTitle from '../../components/PageTitle/PageTitle';
import './ChooseTool.css';

class ChooseTool extends Component {
	constructor(props) {
		super(props);

		this.state = {
			toolType: null,
			crits: [],
			values: [],
		};
	}
	componentDidMount() {
		let crits = [];

		Axios.get(`/criteria/getAllChoosingCriterias`, {
			params: {
				month: this.state.toolType,
			},
		}).then(response => {
			crits = response.data.criterias;

			this.setState({ crits: crits });
		});
	}

	selectGui = () => {
		let crits = [];

		Axios.get(`/criteria/getAllChoosingCriterias`, {
			params: {
				type: 'GUI',
			},
		}).then(response => {
			crits = response.data.criterias;

			this.setState({ crits: crits });
		});

		this.setState({
			toolType: 'GUI',
		});
	};

	selectApi = () => {
		let crits = [];

		Axios.get(`/criteria/getAllChoosingCriterias`, {
			params: {
				type: 'API',
			},
		}).then(response => {
			crits = response.data.criterias;

			this.setState({ crits: crits });
		});

		this.setState({
			toolType: 'API',
		});
	};

	async getData(values) {
		let tools;
		await Axios.post(`/testTool/calculateScoreForAll`, values).then(
			response => {
				tools: response.data;
			},
		);
		return tools;
	}

	async calculateTools(values) {
		let tools = await this.getData(values);
		return tools;
	}

	handleSubmit = () => {
		let critIds = document.querySelectorAll('[optionid]');
		let price = document.getElementById('priceId').value;
		let tools;
		let values = [];

		for (let i = 0; i < critIds.length; i++) {
			if (critIds[i].checked == true) {
				let weight = critIds[i].nextSibling.nextSibling.value;
				if (weight !== '') {
					values.push({
						critValueId: critIds[i].getAttribute('optionid'),
						weight: weight,
					});
				}
			}
		}
		if (price) {
			let priceWeight = document.getElementById('priceWeightId').value;
			values.push({ price: price, weight: priceWeight });
		} else {
			price = '0';
			let priceWeight = document.getElementById('priceWeightId').value;
			values.push({ price: price, weight: priceWeight });
		}

		Axios.post(`/testTool/calculateScoreForAll`, values, {
			params: {
				type: this.state.toolType,
			},
		}).then(response => {
			tools = response.data.toolScores;

			this.props.history.push({
				pathname: '/ResultPage',
				state: {
					tools: tools,
				},
			});
		});
	};

	incrementFooBy(critId, value) {
		return (previousState, currentProps) => {
			return {
				...previousState,
				values: { ...previousState.values, [critId]: value },
			};
		};
	}

	addCriteriaValue = (critId, value) => {
		this.setState(this.incrementFooBy(critId, value));
	};

	addCriteriaValueMulti = (critId, value) => {
		this.setState({
			values: { ...this.state.values, [critId]: value },
		});
	};

	render() {
		const crits = this.state.crits;
		let saveButton;

		if (this.state.toolType != null) {
			saveButton = (
				<button
					onClick={this.handleSubmit}
					className="saveButton text-uppercase btn btn-primary"
				>
					Vyhodnotit
				</button>
			);
		}

		return (
			<div className="col-md-4 container white-box">
				<h1>Vybrat nástroj</h1>
				<div className="row buttons">
					<button
						onClick={this.selectGui}
						className="text-uppercase btn  col-sm-5 diffButton  btn-primary"
					>
						GUI
					</button>

					<button
						onClick={this.selectApi}
						className="text-uppercase btn  col-sm-5 diffButton btn-primary"
					>
						API
					</button>
				</div>

				<div id="crits">
					{this.state.crits ? (
						crits.map(crit => (
							<PickableCriteria
								key={crit.CRITERIA_ID}
								criteria={crit}
								addCriteriaValue={this.addCriteriaValue}
								addCriteriaValueMulti={
									this.addCriteriaValueMulti
								}
							/>
						))
					) : (
						<div />
					)}
				</div>
				{saveButton}
			</div>
		);
	}
}

export default ChooseTool;
