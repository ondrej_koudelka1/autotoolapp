import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { Button } from 'bootstrap';
import { EditableCriteria } from '../../components/Criteria/EditableCriteria';

import FontIcon from '../../components/FontIcon/FontIcon';
import Axios from '../../axios';
import PageTitle from '../../components/PageTitle/PageTitle';

import './NewTool.css';

class NewTool extends Component {
	constructor(props) {
		super(props);

		this.state = {
			toolType: null,
			crits: [],
			values: [],
			valuesMulti: [],
		};

		//this.addCriteriaValue = this.addCriteriaValue.bind(this);
	}
	componentDidMount() {
		let crits = [];

		Axios.get(`/criteria/getAllCriterias`, {
			params: {
				month: this.state.toolType,
			},
		}).then(response => {
			crits = response.data.criterias;

			this.setState({ crits: crits });
		});
	}

	selectGui = () => {
		let crits = [];

		Axios.get(`/criteria/getAllCriterias`, {
			params: {
				type: 'GUI',
			},
		}).then(response => {
			crits = response.data.criterias;

			this.setState({ crits: crits });
		});

		this.setState({
			toolType: 'GUI',
		});
	};

	selectApi = () => {
		let crits = [];

		Axios.get(`/criteria/getAllCriterias`, {
			params: {
				type: 'API',
			},
		}).then(response => {
			crits = response.data.criterias;

			this.setState({ crits: crits });
		});

		this.setState({
			toolType: 'API',
		});
	};

	handleSubmit = () => {
		let name = document.getElementById('testToolName').value;
		let desc = document.getElementById('testToolDesc').value;
		console.log(name, desc);
		Axios.post(`/testTool/createTool`, {
			name: name,
			desc: desc,
			values: this.state.values,
			type: this.state.toolType,
		})
			.then(response => {
				this.props.history.push({
					pathname: '/',
				});
			})
			.catch(function(res) {});
	};

	incrementFooBy(critId, value) {
		return (previousState, currentProps) => {
			return {
				...previousState,
				values: { ...previousState.values, [critId]: value },
			};
		};
	}

	addCriteriaValue = (critId, value) => {
		this.setState(this.incrementFooBy(critId, value));
		console.log(critId, value);
	};

	addCriteriaValueMulti = (critId, value) => {
		this.setState({
			values: { ...this.state.values, [critId]: value },
		});
	};

	render() {
		const crits = this.state.crits;
		let header;
		let saveButton;
		if (crits != null) {
			header = (
				<div className="criteria">
					<div className="row header-item form-group ">
						<label className="header-label col-sm-2">Název</label>
						<input
							type="text"
							maxlength="50"
							id="testToolName"
							className="form-control"
						/>
					</div>
					<div className="row header-item">
						<label className="header-label col-sm-2">Popis</label>
						<textarea
							maxlength="1000"
							id="testToolDesc"
							className="form-control"
							rows="5"
						/>
					</div>
				</div>
			);

			if (this.state.toolType != null) {
				saveButton = (
					<button
						onClick={this.handleSubmit}
						className="saveButton btn btn-primary"
					>
						Uložit
					</button>
				);
			}
		}

		return (
			<div className="col-md-4 container white-box">
				<h1>Přidat nástroj</h1>
				<div className="row buttons">
					<button
						onClick={this.selectGui}
						className="text-uppercase btn  col-sm-5 diffButton btn-primary"
					>
						GUI
					</button>

					<button
						onClick={this.selectApi}
						className="text-uppercase btn  col-sm-5 diffButton btn-primary"
					>
						API
					</button>
				</div>
				{header}
				<div id="crits">
					{this.state.crits ? (
						crits.map(crit => (
							<EditableCriteria
								key={crit.CRITERIA_ID}
								criteria={crit}
								addCriteriaValue={this.addCriteriaValue}
								addCriteriaValueMulti={
									this.addCriteriaValueMulti
								}
							/>
						))
					) : (
						<div />
					)}
				</div>
				{saveButton}
			</div>
		);
	}
}

export default NewTool;
