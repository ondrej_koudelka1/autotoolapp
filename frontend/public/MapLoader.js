window.MapLoader = (function () {
  var ready = false;
  var listeners = [];

  return {
    ready: function (fn) {
      ready ? fn() : listeners.push(fn);
    },
    notify: function () {
      ready = true;
      listeners.forEach(fn => fn());
    }
  }
})();
