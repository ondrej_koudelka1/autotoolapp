# Let's run

Let's run is an application that connects people whose hobby is running and who don't enjoy running alone.

## Start the project
For convenience, there is `yarn start` command that installs frontend and backend packages, runs database
migrations and starts development server ([http://localhost:3000](http://localhost:3000) is opened).

A more detailed overview of commands can be found in the [frontend](./frontend/README.md),
respectively the [backend](./backend/README.md) readmes.

The application needs environment variables to be set. This is done by running the command below that
simply copies the file `.env.example` to `.env`.
```sh
cp .env.example .env
```
There are two variables - `NODE_ENV` and `SECRET`. The first one specifies the current environment used across
the app (eg. `development`, `testing`). The purpose of the second one is to secure the generated tokens.

Both of them may be left unchanged.