import dotenv from 'dotenv-override';
import path from 'path';
import { app } from './server';
import { setupLogging } from './logging';

import {rootDir} from '../../frontend/src/paths';

dotenv.config({
	path: path.resolve(rootDir, '.env'),
	override: true,
});
const { PORT = 3030 } = process.env;

setupLogging();

app.listen(PORT, () => {
	console.log(`Server started on http://localhost:${PORT}!`);
});
