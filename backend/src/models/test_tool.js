'use strict';
module.exports = (sequelize, DataTypes) => {
	const TestTool = sequelize.define(
		'TestTool',
		{
			TEST_TOOLS_ENUM_ID: {
				type: DataTypes.INTEGER,
				allowNull: false,
				primaryKey: true,
				autoIncrement: true,
			},
			NAME: {
				type: DataTypes.STRING(50),
				allowNull: false,
			},
			DESC: {
				type: DataTypes.STRING(1000),
				allowNull: false,
			},
			TYPE: {
				type: DataTypes.STRING(10),
				allowNull: false,
			},
		},
		{
			tableName: 'TEST_TOOLS_ENUM',
			underscored: false,
			timestamps: false,
		},
	);
	TestTool.associate = function(models) {
		TestTool.belongsToMany(models.CritValue, {
			as: 'TestToolsRated',
			through: models.TestToolsCriteria,
			foreignKey: 'TEST_TOOLS_ENUM_ID',
		});
	};

	return TestTool;
};
