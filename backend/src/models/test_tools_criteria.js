'use strict';
module.exports = (sequelize, DataTypes) => {
	const TestToolsCriteria = sequelize.define(
		'TestToolsCriteria',
		{
			TEST_TOOLS_CRITERIA_ID: {
				type: DataTypes.INTEGER,
				allowNull: false,
				primaryKey: true,
				autoIncrement: true,
			},
			TEST_TOOLS_ENUM_ID: {
				type: DataTypes.INTEGER,
				allowNull: false,
			},
			CRIT_VALUES_ENUM_ID: {
				type: DataTypes.INTEGER,
				allowNull: false,
			},
		},
		{
			tableName: 'TEST_TOOLS_CRITERIA',
			underscored: false,
			timestamps: false,
		},
	);
	TestToolsCriteria.associate = function(models) {
		TestToolsCriteria.belongsTo(models.TestTool, {
			foreignKey: 'TEST_TOOLS_ENUM_ID',
		});
		TestToolsCriteria.belongsTo(models.CritValue, {
			foreignKey: 'CRIT_VALUES_ENUM_ID',
		});
	};
	return TestToolsCriteria;
};
