'use strict';
module.exports = (sequelize, DataTypes) => {
	const CritValue = sequelize.define(
		'CritValue',
		{
			CRIT_VALUES_ENUM_ID: {
				type: DataTypes.INTEGER,
				allowNull: false,
				primaryKey: true,
				autoIncrement: true,
			},
			CRIT_VALUE: {
				type: DataTypes.STRING(50),
				allowNull: false,
			},
			CRITERIA_ID: {
				type: DataTypes.INTEGER,
				allowNull: false,
			},
		},
		{
			tableName: 'CRIT_VALUES_ENUM',
			underscored: false,
			timestamps: false,
		},
	);
	CritValue.associate = function(models) {
		CritValue.belongsTo(models.TestToolsCriteria, {
			foreignKey: 'CRIT_VALUES_ENUM_ID',
		});

		CritValue.belongsToMany(models.TestTool, {
			as: 'TestToolsRatings',
			through: models.TestToolsCriteria,
			foreignKey: 'CRIT_VALUES_ENUM_ID',
		});

		CritValue.belongsTo(models.Criteria, {
			foreignKey: 'CRITERIA_ID',
			as: 'criteriaValue',
		});
	};
	return CritValue;
};
