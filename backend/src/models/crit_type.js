'use strict';
module.exports = (sequelize, DataTypes) => {
	const CritType = sequelize.define(
		'CritType',
		{
			CRIT_TYPE_ID: {
				type: DataTypes.INTEGER,
				allowNull: false,
				primaryKey: true,
				autoIncrement: true,
			},
			NAME: {
				type: DataTypes.STRING(50),
				allowNull: false,
			},
		},
		{
			tableName: 'CRIT_TYPE_ENUM',
			timestamps: false,
			//underscored: false,
		},
	);
	CritType.associate = function(models) {
		CritType.hasMany(models.Criteria, { foreignKey: 'CRIT_TYPE_ID' });
	};

	return CritType;
};
