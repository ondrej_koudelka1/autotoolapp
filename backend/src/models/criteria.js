'use strict';
module.exports = (sequelize, DataTypes) => {
	const Criteria = sequelize.define(
		'Criteria',
		{
			CRITERIA_ID: {
				type: DataTypes.INTEGER,
				allowNull: false,
				primaryKey: true,
				autoIncrement: true,
			},
			NAME: {
				type: DataTypes.STRING(50),
				allowNull: false,
			},
			CODE: {
				type: DataTypes.STRING(10),
				allowNull: false,
			},
			API: {
				type: DataTypes.BOOLEAN,
				allowNull: false,
			},
			GUI: {
				type: DataTypes.BOOLEAN,
				allowNull: false,
			},
			DESC: {
				type: DataTypes.STRING(250),
				allowNull: false,
			},
			CHOOSE_DESC: {
				type: DataTypes.STRING(250),
				allowNull: false,
			},
			CRIT_TYPE_ID: {
				type: DataTypes.INTEGER,
				allowNull: false,
			},
		},

		{
			tableName: 'CRITERIA',
			underscored: false,
			timestamps: false,
		},

		{
			hooks: {
				beforeValidate: function(Criteria, options) {
					Criteria.value = 'aaa';
				},
			},
		},
	);
	Criteria.associate = function(models) {
		Criteria.hasMany(models.CritValue, {
			foreignKey: 'CRITERIA_ID',
			as: 'critValue',
		});

		Criteria.belongsTo(models.CritType, {
			foreignKey: 'CRIT_TYPE_ID',
		});
	};

	return Criteria;
};
