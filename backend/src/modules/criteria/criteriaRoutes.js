import expressAsyncAwait from 'express-async-await';
import { Router } from 'express';

import { getAllCriterias } from './criteriaController.js';
import { getApiCriterias } from './criteriaController.js';
import { getGuiCriterias } from './criteriaController.js';
import { addCriteriaValue } from './criteriaController.js';
import { getAllChoosingCriterias } from './criteriaController.js';

const router = expressAsyncAwait(Router());
router.get('/getAllCriterias', getAllCriterias);
router.get('/getApiCriterias', getApiCriterias);
router.get('/getGuiCriterias', getGuiCriterias);
router.post('/addCriteriaValue', addCriteriaValue);
router.get('/getAllChoosingCriterias', getAllChoosingCriterias);

export default router;
