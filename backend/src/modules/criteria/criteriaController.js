import db from '../../models';

const Criteria = db.Criteria;
const CritType = db.CritType;
const CritValue = db.CritValue;

export const getAllCriterias = async (req, res) => {
	let type = req.query.type;
	let criterias;

	if (type === 'API') {
		criterias = await Criteria.findAll({
			where: {
				API: 1,
			},
			attributes: ['CRITERIA_ID', 'NAME', 'DESC', 'CODE'],
			include: [
				{
					model: CritType,
				},
				{
					model: CritValue,
					as: 'critValue',
					attributes: ['CRIT_VALUE', 'CRIT_VALUES_ENUM_ID'],
				},
			],
			order: [['CODE']],
		});
	} else if (type === 'GUI') {
		criterias = await Criteria.findAll({
			where: {
				GUI: 1,
			},
			attributes: ['CRITERIA_ID', 'NAME', 'DESC', 'CODE'],
			include: [
				{
					model: CritType,
				},
				{
					model: CritValue,
					as: 'critValue',
					attributes: ['CRIT_VALUE', 'CRIT_VALUES_ENUM_ID'],
				},
			],
			order: [['CODE']],
		});
	}

	return res.json({ criterias });
};

export const getAllChoosingCriterias = async (req, res) => {
	let type = req.query.type;
	let criterias;

	if (type === 'API') {
		criterias = await Criteria.findAll({
			where: {
				API: 1,
			},
			attributes: ['CRITERIA_ID', 'NAME', 'DESC', 'CODE', 'CHOOSE_DESC'],
			include: [
				{
					model: CritType,
				},
				{
					model: CritValue,
					as: 'critValue',
					attributes: ['CRIT_VALUE', 'CRIT_VALUES_ENUM_ID'],
					where: {
						CRIT_VALUE: { $not: 'false' },
					},
				},
			],
			order: [['CODE']],
		});
	} else if (type === 'GUI') {
		criterias = await Criteria.findAll({
			where: {
				GUI: 1,
			},
			attributes: ['CRITERIA_ID', 'NAME', 'DESC', 'CODE', 'CHOOSE_DESC'],
			include: [
				{
					model: CritType,
				},
				{
					model: CritValue,
					as: 'critValue',
					attributes: ['CRIT_VALUE', 'CRIT_VALUES_ENUM_ID'],
					where: {
						CRIT_VALUE: { $not: 'false' },
					},
				},
			],
			order: [['CODE']],
		});
	}

	return res.json({ criterias });
};

export const getApiCriterias = async (req, res) => {
	let criterias = await Criteria.findAll({
		where: {
			API: 1,
		},
		attributes: ['CRITERIA_ID', 'NAME', 'DESC', 'CODE'],
		include: [
			{
				model: CritType,
			},
			{
				model: CritValue,
				as: 'critValue',
				attributes: ['CRIT_VALUE'],
			},
		],
		order: [['CODE']],
	});

	return res.json({ criterias });
};

export const getGuiCriterias = async (req, res) => {
	const critId = req.query.critId;
	let criterias = await Criteria.findAll({
		where: {
			GUI: 1,
		},
		attributes: ['CRITERIA_ID', 'NAME', 'DESC', 'CODE'],
		include: [
			{
				model: CritType,
			},
			{
				model: CritValue,
				as: 'critValue',
				attributes: ['CRIT_VALUE'],
			},
		],
		order: [['CODE']],
	});

	return res.json({ criterias: criterias });
};

export const getCriteriaValue = async critId => {
	let critValues = await CritValue.findAll({
		where: {
			CRITERIA_ID: critId,
		},
	});

	return critValues;
};

export const addCriteriaValue = async (req, res) => {
	const critId = req.body.critId;
	const value = req.body.value;
	let critIdCreated;

	await CritValue.create({
		CRITERIA_ID: critId,
		CRIT_VALUE: value,
	}).then(function(crit) {
		critIdCreated = crit.CRIT_VALUES_ENUM_ID;
	});

	return res.json({ id: critIdCreated });
};

export const getCriteriaValues = async (req, res) => {
	const critId = req.query.critId;

	await CritValue.findAll({
		attributes: ['CRIT_VALUE'],

		where: {
			CRITERIA_ID: critId,
		},
	});
};
