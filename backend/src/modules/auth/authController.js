import bcrypt from 'bcrypt';

import {getUserByEmail, createUser} from '../user/userController';
import {createToken} from './tokener';

export const WRONG_EMAIL_OR_PASSWORD = 'E-mail or password is wrong.';
export const INVALID_DATA = 'Your data are invalid.';
export const EMAIL_EXISTS = 'This E-mail already exists.';
export const GENERAL_ERROR = 'Something went wrong. Please try it again later.';

export const signUpUser = async (req, res) => {
	const {email, firstName, lastName, password, lat, lng} = req.body;
	if (email && firstName && lastName && password) {
		const user = await getUserByEmail(email);

		if (user) {
			res.status(409).json({
				error: EMAIL_EXISTS,
			});
		} else {
			createUser({
				email,
				firstName,
				lastName,
				password,
				lat,
				lng,
			})
				.then(user => {
					const {id, email, firstName, lastName} = user;
					res.status(200).json({
						user: {
							id,
							email,
							firstName,
							lastName,
						},
					});
				})
				.catch(error => {
					console.log('authController createUser(): ', error);
					res.status(400).json({error: GENERAL_ERROR});
				});
		}
	} else {
		res.status(400).json({
			error: INVALID_DATA,
		});
	}
};

export const loginUser = async (req, res) => {
	const {email, password} = req.body;
	const user = await getUserByEmail(email);
	const passwordIsValid = user && bcrypt.compareSync(password, user.password);

	if (passwordIsValid) {
		const {
			id,
			email,
			firstName,
			lastName,
			startingPosition: position,
		} = user;
		const token = createToken(id);
		const {coordinates} = position || {};
		const startingPosition = coordinates
			? {
				lat: coordinates[0],
				lng: coordinates[1],
			}
			: null;
		res.status(200).json({
			user: {
				id,
				email,
				firstName,
				lastName,
				startingPosition,
				token,
			},
		});
	} else {
		res.status(400).json({
			error: WRONG_EMAIL_OR_PASSWORD,
		});
	}
};
