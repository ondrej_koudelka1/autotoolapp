import expressAsyncAwait from 'express-async-await';
import { Router } from 'express';

import { createTool } from './testToolsController.js';
import { rateTool } from './testToolsController.js';
import { testScore } from './testToolsController.js';
import { calculateScoreForAll } from './testToolsController.js';
import { getAllTools } from './testToolsController.js';

const router = expressAsyncAwait(Router());
router.post('/createTool', createTool);
router.post('/rateTool', rateTool);
router.post('/testScore', testScore);
router.post('/calculateScoreForAll', calculateScoreForAll);
router.get('/getAllTools', getAllTools);

export default router;
