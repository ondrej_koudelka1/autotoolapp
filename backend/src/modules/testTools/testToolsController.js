import db from '../../models';

const TestTool = db.TestTool;
const TestToolsCriteria = db.TestToolsCriteria;
const CritValue = db.CritValue;
const Criteria = db.Criteria;

export const createTool = async (req, res) => {
	let name = req.body.name;
	let desc = req.body.desc;
	let values = req.body.values;
	let type = req.body.type;
	let toolId;
	let critIds = [];

	await TestTool.create({
		NAME: name,
		DESC: desc,
		TYPE: type,
	}).then(function(tool) {
		toolId = tool.TEST_TOOLS_ENUM_ID;
	});

	Object.keys(values).forEach(function(key) {
		var val = values[key];
		if (val !== 'false') {
			critIds.push({ critId: key });
		}
	});

	await rateTool(toolId, critIds);

	return res.json({ Result: 'ok' });
};

export const rateTool = async (toolId, critIds) => {
	for (let i = 0; i < critIds.length; i++) {
		let critId = critIds[i].critId;

		await TestToolsCriteria.create({
			TEST_TOOLS_ENUM_ID: toolId,
			CRIT_VALUES_ENUM_ID: critId,
		});
	}

	return 0;
};

export const testScore = async (req, res) => {
	let toolId = '1';
	let crits = [
		{ critValueId: 2, weight: 5 },
		{ critValueId: 3, weight: 6 },
		{ critValueId: 4, weight: 7 },
		{ critValueId: 5, weight: 8 },
	];

	let result = await calculateScoreForTool(toolId, crits);
	return res.json({ result: result });
};

export const calculateScoreForAll = async (req, res) => {
	let crits = req.body;
	let type = req.query.type;

	let toolScores = [];
	let tools = await TestTool.findAll({
		where: {
			TYPE: type,
		},
		attributes: ['TEST_TOOLS_ENUM_ID', 'NAME', 'DESC'],
	});

	for (let i = 0; i < tools.length; i++) {
		let toolScore = await calculateScoreForTool(
			tools[i].TEST_TOOLS_ENUM_ID,
			crits,
		);
		let toolCrits = await TestTool.findOne({
			where: { TEST_TOOLS_ENUM_ID: tools[i].TEST_TOOLS_ENUM_ID },
			include: [
				{
					model: CritValue,
					as: 'TestToolsRated',

					include: [
						{
							model: Criteria,
							as: 'criteriaValue',
						},
					],
				},
			],
		});

		let criterias = await Criteria.findAll({
			include: [
				{
					model: CritValue,
					as: 'critValue',
					include: [
						{
							model: TestTool,
							as: 'TestToolsRatings',
							where: {
								TEST_TOOLS_ENUM_ID: tools[i].TEST_TOOLS_ENUM_ID,
							},
						},
					],
				},
			],
		});

		toolScores.push({
			toolId: tools[i].TEST_TOOLS_ENUM_ID,
			toolName: tools[i].NAME,
			toolDesc: tools[i].DESC,
			toolScore: toolScore,
			toolCrits: toolCrits,
			criterias: criterias,
		});
	}

	if (toolScores != null) {
		toolScores.sort(function(a, b) {
			return b.toolScore - a.toolScore;
		});
	}

	return res.json({ toolScores });
};

export const calculateScoreForTool = async (toolId, crits) => {
	let finalScore = parseInt('0');
	let maxPrice;
	let priceWeight;

	for (let i = 0; i < crits.length; i++) {
		let weight = parseInt(crits[i].weight);
		let critValueId = crits[i].critValueId;
		if (crits[i].price != null) {
			maxPrice = crits[i].price;
			priceWeight = parseInt(crits[i].weight);
		}

		let checker = await TestToolsCriteria.count({
			where: {
				TEST_TOOLS_ENUM_ID: toolId,
				CRIT_VALUES_ENUM_ID: critValueId,
			},
		});

		if (checker > 0) {
			finalScore = finalScore + weight;
		}
	}

	let price = await CritValue.findOne({
		attributes: ['CRIT_VALUE'],
		include: [
			{
				model: Criteria,
				as: 'criteriaValue',
				where: {
					CODE: 'CRIT_01',
				},
			},
			{
				model: TestTool,
				as: 'TestToolsRatings',
				where: { TEST_TOOLS_ENUM_ID: toolId },
			},
		],
	});

	if (price != null) {
		if (parseInt(price.CRIT_VALUE) <= parseInt(maxPrice)) {
			finalScore = finalScore + priceWeight;
		}
	} else {
		//finalScore = finalScore + priceWeight;
	}

	return finalScore;
};

export const getAllTools = async (req, res) => {
	let result = [];
	let tools = await TestTool.findAll({
		attributes: ['TEST_TOOLS_ENUM_ID', 'NAME', 'DESC', 'TYPE'],
	});

	for (let i = 0; i < tools.length; i++) {
		let criterias = await Criteria.findAll({
			include: [
				{
					model: CritValue,
					as: 'critValue',
					include: [
						{
							model: TestTool,
							as: 'TestToolsRatings',
							where: {
								TEST_TOOLS_ENUM_ID: tools[i].TEST_TOOLS_ENUM_ID,
							},
						},
					],
				},
			],
		});

		result.push({
			toolId: tools[i].TEST_TOOLS_ENUM_ID,
			toolName: tools[i].NAME,
			toolDesc: tools[i].DESC,
			toolType: tools[i].TYPE,
			criterias: criterias,
		});
	}

	return res.json({
		result,
	});
};
