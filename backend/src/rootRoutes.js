import { Router } from 'express';

import testToolRoutes from './modules/testTools/testToolRoutes';
import criteriaRoutes from './modules/criteria/criteriaRoutes';

import { verifyToken } from './modules/auth/tokener';

const router = Router();

router.use('/api/testTool', testToolRoutes);
router.use('/api/criteria', criteriaRoutes);

export default router;
