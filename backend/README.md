# Backend
Backend part of the Letsrun app.

## Requirements
- [Node.js](https://nodejs.org/en/) v6.11 or later
- `yarn` (`npm install --global yarn`)
- MySQL 5.7 or later, MariaDB 10.2 or later - database must support spatial data types, functions and indexes

## Installation
```sh
yarn install
```

## Config database connection
```sh
cp src/config/config.json.example src/config/config.json 
```

Set database credentials for the `development`, `test` and `production` environment.

## Run migrations
```sh
yarn migrate
```

## Seed example data
```sh
yarn seed
```

## Run local dev server
```sh
yarn dev
```

## Run local production server
```sh
yarn start
```

Builds and starts the server.

## Production build
```sh
yarn build
```

## Production build watch
```sh
yarn build:watch
```

Runs build each time `.js` file in `./src` is changed.

**Useful for `*.vse.handson.pro` hosting.**

## See server logs
```sh
tail -F log/app.log
```
